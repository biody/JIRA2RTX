
// JIRA2RTXDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "JIRA2RTX.h"
#include "JIRA2RTXDlg.h"
#include "afxdialogex.h"
#include "RWFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_MSG_ADDLOG WM_APP+1
string status_wait = "等待处理";
string status_running = "进行中";
string status_reopen = "重置";
string status_check = "等待验证";
string status_close = "关闭";
string status_fixed = "已修复";
string jrseverip = "192.168.8.81";
int jrseverport = 8080;
string rtxseverip = "192.168.1.166";
int rtxseverport = 8012;
string os_username = "zhupf(QQ335264417)";
string os_password = "1";
string jrproject = "mygame";

map<string, string> g_kUserNameJira2Rtx;

string AnsiToUtf8(const string& _AnsiStr)
{
	CA2W _W((LPCSTR)_AnsiStr.c_str());
	CW2A _A((LPCWSTR)_W,CP_UTF8);
	return string((LPCSTR)_A);
}

string Utf8ToAnsi(const string& _Utf8Str)
{
	CA2W _W((LPCSTR)_Utf8Str.c_str(),CP_UTF8);
	CW2A _A((LPCWSTR)_W);
	return string((LPCSTR)_A);
}

bool s_UrlChars[256];
bool IsSafeUrlChar(char _chCheck) {
	static bool bIsSafeUrlCharInited = false;
	if (!bIsSafeUrlCharInited) {
		memset(s_UrlChars,true,256);
		static char _UnsafeChars[] = "#%^`[]{}; ";
		char* pCh = _UnsafeChars;
		while (*pCh) {
			s_UrlChars[*pCh++] = false;
		}
		bIsSafeUrlCharInited = true;
	}
	return s_UrlChars[_chCheck];
}

string UrlEncode(const string& kInStr)
{
	static char Hex[] = "0123456789abcdef";
	string kInStrUtf8 = AnsiToUtf8(kInStr);
	char* pUTF8Url = (char*)kInStrUtf8.c_str();
	if (!pUTF8Url || kInStr == kInStrUtf8) {
		return kInStr;
	}
	size_t iUTF8UrlLen = kInStrUtf8.size();
	string kEncodedStr;
	kEncodedStr.reserve(iUTF8UrlLen*3);
	unsigned char cCh;
	for (int _Idx = 0; _Idx < (int)iUTF8UrlLen; _Idx++) {
		cCh = *pUTF8Url;
		if (IsSafeUrlChar(cCh) && isprint(cCh)) {
			kEncodedStr += *pUTF8Url;
		}
		else {
			kEncodedStr += '%';
			kEncodedStr += Hex[(cCh>>4)&0x0F];
			kEncodedStr += Hex[cCh&0x0F];
		}
		pUTF8Url++;
	}
	return kEncodedStr;
}

string& replace_all(string& _SrcStr,const string& _OldStr,const string& _NewStr)
{
	size_t _Pos = 0;
	while((_Pos=_SrcStr.find(_OldStr))!=string::npos) {
		_SrcStr.replace(_Pos,_OldStr.length(),_NewStr);
	}
	return _SrcStr;
}

string& replace_all_distinct(string& _SrcStr,const string& _OldStr,const string& _NewStr)
{
	size_t _Pos = 0;
	while((_Pos=_SrcStr.find(_OldStr,_Pos))!=string::npos) {
		_SrcStr.replace(_Pos,_OldStr.length(),_NewStr);
		_Pos+=_NewStr.length();
	}
	return _SrcStr;
}

int fast_split_by(const string& _SrcStr, vector<string>& _StrList, char _ChDelim)
{
	_StrList.clear();
	if(_SrcStr.empty()){
		return 0;
	}

	INT _Length = (INT)_SrcStr.size();
	string _TempString = "";
	CONST CHAR* _pStrData = _SrcStr.c_str();
	for(INT _Pos = 0; _Pos < _Length; _Pos++){
		if(_pStrData[_Pos] == _ChDelim){
			_StrList.push_back(_TempString);
			_TempString.clear();
			continue;
		}
		_TempString += _pStrData[_Pos];
	}
	_StrList.push_back(_TempString);
	return (int)_StrList.size();
}
string& trim_left(string& _SrcStr,string strTirmChars = " ")
{
	if (_SrcStr.empty()) return _SrcStr;
	for (int i = 0; i < (INT)_SrcStr.size(); i++) {
		if (strTirmChars.find(_SrcStr[i]) == string::npos) {
			_SrcStr = _SrcStr.substr(i,_SrcStr.size()-i+1);
			return _SrcStr;
		}
	}
	_SrcStr.clear();
	return _SrcStr;
}

string& trim_right(string& _SrcStr,string strTirmChars = " ")
{
	if (_SrcStr.empty()) return _SrcStr;
	for (int i((INT)_SrcStr.size()-1); i >= 0; i--) {
		if (strTirmChars.find(_SrcStr[i]) == string::npos) {
			_SrcStr = _SrcStr.substr(0,i+1);
			return _SrcStr;
		}
	}
	_SrcStr.clear();
	return _SrcStr;
}

string& trim(string& _SrcStr,string strTirmChars = " ")
{
	trim_left(_SrcStr,strTirmChars);
	trim_right(_SrcStr,strTirmChars);
	return _SrcStr;
}

string GetCurWorkPath()
{
	TCHAR _FullPathBuf[MAX_PATH] = {0};
	GetModuleFileName(NULL, _FullPathBuf, MAX_PATH);
	::PathRemoveFileSpec(_FullPathBuf);
	return string((LPSTR)CW2A(_FullPathBuf));
}

//加载帐号映射表
void LoadUserNameJira2Rtx()
{
	string kJira2rtxData;
	if (RWFile::LoadFile("user_jira2rtx.txt", kJira2rtxData))
	{
		replace_all(kJira2rtxData, "\r\n", "\n");
		replace_all(kJira2rtxData, "\n\n", "\n");
		vector<string> _StrList;
		fast_split_by(kJira2rtxData, _StrList, '\n');
		vector<string> jirartxUser;
		for (int i = 0; i < (int)_StrList.size(); i++)
		{
			string& line = _StrList[i];
			if (line.empty() || line[0] == ';')
			{
				continue;
			}
			if (fast_split_by(line, jirartxUser, '=') == 2)
			{
				g_kUserNameJira2Rtx.insert(std::make_pair(trim(jirartxUser[0]),trim(jirartxUser[1])));
			}
		}
	}
}

void ReadIni()
{
	LoadUserNameJira2Rtx();

	string kIniFile = GetCurWorkPath() + "\\JIRA2RTX.ini";
	string kTemp;
	kTemp.resize(512,NULL);
	GetPrivateProfileStringA("设置","jrproject",jrproject.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());jrproject = kTemp.c_str();
	GetPrivateProfileStringA("设置","os_username",os_username.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());os_username = kTemp.c_str();
	GetPrivateProfileStringA("设置","os_password",os_password.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());os_password = kTemp.c_str();

	GetPrivateProfileStringA("设置","jrseverip",jrseverip.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());jrseverip = kTemp.c_str();
	jrseverport = GetPrivateProfileIntA("设置","jrseverport",jrseverport,kIniFile.c_str());
	GetPrivateProfileStringA("设置","rtxseverip",rtxseverip.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());rtxseverip = kTemp.c_str();
	rtxseverport = GetPrivateProfileIntA("设置","rtxseverport",rtxseverport,kIniFile.c_str());

	GetPrivateProfileStringA("翻译","status_wait",status_wait.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());status_wait = kTemp.c_str();
	GetPrivateProfileStringA("翻译","status_running",status_running.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());status_running = kTemp.c_str();
	GetPrivateProfileStringA("翻译","status_reopen",status_reopen.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());status_reopen = kTemp.c_str();
	GetPrivateProfileStringA("翻译","status_check",status_check.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());status_check = kTemp.c_str();
	GetPrivateProfileStringA("翻译","status_close",status_close.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());status_close = kTemp.c_str();
	GetPrivateProfileStringA("翻译","status_fixed",status_fixed.c_str(),(LPSTR)kTemp.c_str(),512,kIniFile.c_str());status_fixed = kTemp.c_str();
}

void SaveIni()
{
	string kIniFile = GetCurWorkPath() + "\\JIRA2RTX.ini";
	CStringA kTemp;
	WritePrivateProfileStringA("设置","jrproject",jrproject.c_str(),kIniFile.c_str());
	WritePrivateProfileStringA("设置","os_username",os_username.c_str(),kIniFile.c_str());
	WritePrivateProfileStringA("设置","os_password",os_password.c_str(),kIniFile.c_str());

	WritePrivateProfileStringA("设置","jrseverip",jrseverip.c_str(),kIniFile.c_str());
	kTemp.Format("%d",jrseverport);
	WritePrivateProfileStringA("设置","jrseverport",kTemp,kIniFile.c_str());
	WritePrivateProfileStringA("设置","rtxseverip",rtxseverip.c_str(),kIniFile.c_str());
	kTemp.Format("%d",rtxseverport);
	WritePrivateProfileStringA("设置","rtxseverport",kTemp,kIniFile.c_str());

	WritePrivateProfileStringA("翻译","status_wait",status_wait.c_str(),kIniFile.c_str());
	WritePrivateProfileStringA("翻译","status_running",status_running.c_str(),kIniFile.c_str());
	WritePrivateProfileStringA("翻译","status_reopen",status_reopen.c_str(),kIniFile.c_str());
	WritePrivateProfileStringA("翻译","status_check",status_check.c_str(),kIniFile.c_str());
	WritePrivateProfileStringA("翻译","status_close",status_close.c_str(),kIniFile.c_str());
	WritePrivateProfileStringA("翻译","status_fixed",status_fixed.c_str(),kIniFile.c_str());
}

void* WINAPI ResolveWorkThread(void* lpParam);


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CJIRA2RTXDlg 对话框




CJIRA2RTXDlg::CJIRA2RTXDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CJIRA2RTXDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_StartAt = 0;
	ReadIni();
	SaveIni();
}

void CJIRA2RTXDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_RUN, m_BtnRun);
	DDX_Control(pDX, IDC_LIST_LOG, m_LogList);
}

BEGIN_MESSAGE_MAP(CJIRA2RTXDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_RUN, &CJIRA2RTXDlg::OnBnClickedButtonRun)
	ON_MESSAGE(WM_MSG_ADDLOG,&CJIRA2RTXDlg::OnAddLog)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CJIRA2RTXDlg 消息处理程序

BOOL CJIRA2RTXDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	GetClientRect(&m_WndRect);
	m_BtnRun.GetWindowRect(&m_RectBtnRun);
	ScreenToClient(&m_RectBtnRun);
	m_LogList.GetWindowRect(&m_RectLogList);
	ScreenToClient(&m_RectLogList);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CJIRA2RTXDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CJIRA2RTXDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CJIRA2RTXDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CJIRA2RTXDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if (cx == 0 || cy == 0)
	{
		return;
	}
	if (!m_BtnRun.GetSafeHwnd())
	{
		return;
	}
	int xDiff = cx - m_WndRect.Width();
	int yDiff = cy - m_WndRect.Height();
	CRect kRect;
	kRect = m_RectBtnRun;
	kRect.left += xDiff;
	kRect.right += xDiff;
	m_BtnRun.MoveWindow(kRect);
	kRect = m_RectLogList;
	kRect.right += xDiff;
	kRect.bottom += yDiff;
	m_LogList.MoveWindow(kRect);
}

void CJIRA2RTXDlg::OnBnClickedButtonRun()
{
	m_BtnRun.EnableWindow(FALSE);
	DWORD dwThreadID;
	HANDLE hThread = ::CreateThread(NULL,NULL,(LPTHREAD_START_ROUTINE)ResolveWorkThread,(LPVOID)this,NULL,&dwThreadID);
	if(hThread)
	{
		CloseHandle(hThread);
	}
}

void* WINAPI ResolveWorkThread(void* lpParam)
{
	CJIRA2RTXDlg* pJIRA2RTXDlg = (CJIRA2RTXDlg*)lpParam;
	if(pJIRA2RTXDlg)
	{
		pJIRA2RTXDlg->DoResolveWorkThread();
	}
	return NULL;
}

void CJIRA2RTXDlg::DoResolveWorkThread()
{
	BOOL bFirst = TRUE;
	while(true)
	{
		if(!IsWindow(GetSafeHwnd()))
		{
			break;
		}
		BOOL bUpUpdatedTime = TRUE;
		BOOL bRet = TRUE;
		while(bRet)
		{
			bRet = QueryUpdatedIssues(bFirst);
			if(bRet)
			{
				ParseIssueData(bUpUpdatedTime,!bFirst);
				bUpUpdatedTime = FALSE;
				bRet = (m_StartAt!=0);
			}
		}
		SendAllNotify();
		m_StartAt = 0;
		m_kNewData.clear();
		bFirst = FALSE;
		Sleep(5000);
	}
}

LRESULT CJIRA2RTXDlg::OnAddLog(WPARAM wParam,LPARAM lParam)
{
	while(m_LogList.GetCount() > 100)
	{
		m_LogList.DeleteString(0);
	}
	wstring* pLogMsg = (wstring*)wParam;
	if(pLogMsg)
	{
		m_LogList.AddString(pLogMsg->c_str());
		CStdioFile file;
		if (file.Open(L"JIRA2RTX.log", CFile::modeCreate | CFile::modeWrite | CFile::modeNoTruncate))
		{
			file.SeekToEnd();
			setlocale( LC_CTYPE, "chs" );
			file.WriteString(pLogMsg->c_str());
			file.WriteString(L"\n");
			setlocale( LC_CTYPE, "C" );
			file.Close();
		}
		delete pLogMsg;
	}
	return NULL;
}

BOOL CJIRA2RTXDlg::RtxNotify(const Issue& kIssue,string kMsgTip, const string& kIssueKey, const string& kReceiver)
{
	CStringA kTemp;
	kTemp.Format("%d", jrseverport);
	kMsgTip += " [";
	kMsgTip += kIssueKey + "|http://" + jrseverip + ":" + (LPCSTR)kTemp + "/browse/" + kIssueKey + "]";
	kMsgTip += " ";
	kMsgTip += kIssue.Cur.summary;
	wstring* pLogMsg = new wstring((LPTSTR)CA2T((kReceiver + " : " + kMsgTip).c_str()));
	PostMessage(WM_MSG_ADDLOG,(WPARAM)pLogMsg);
#ifndef _DEBUG
	SendNotify(kReceiver,kMsgTip);
#endif
	return TRUE;
}

BOOL CJIRA2RTXDlg::SendAllNotify()
{
	string kReceiver;
	string kMsgTip;
	string kIssueKey;
	map<INT,INT>::iterator kIter;
	for(kIter = m_NotifyIssues.begin(); kIter != m_NotifyIssues.end(); ++kIter)
	{
		BOOL bWatcher = FALSE;
		Issue& kIssue = m_Issues[kIter->first];
		kReceiver = kIssue.Cur.assignee;
		kIssueKey = kIssue.key;
		if(kIssue.Cur.created == kIssue.Cur.updated) // 创建
		{
			kMsgTip = "你有新任务！";
		}
		else // 更新
		{
			if(kIssue.Old.reporter != kIssue.Cur.reporter)
			{
				kReceiver = kIssue.Cur.reporter;
				kMsgTip = "任务报告人改变！";
				bWatcher = TRUE;
			}
			else if(kIssue.Old.assignee != kIssue.Cur.assignee)
			{
				kMsgTip = "你有新任务！";
			}
			else if(kIssue.Old.status != kIssue.Cur.status)
			{
				if(kIssue.Cur.status == status_check)
				{
					kMsgTip = "任务等待验证！";
					RtxNotify(kIssue, kMsgTip, kIssueKey, kIssue.Cur.reporter);
				}
				else if(kIssue.Cur.status == status_reopen)
				{
					kMsgTip = "任务被重置！";
				}
				else if(kIssue.Cur.status == status_close && kIssue.Cur.resolution == status_fixed)
				{
					kMsgTip = "任务已完成！";
					bWatcher = TRUE;
				}
				else
				{
					kMsgTip = "任务有更新！";
					bWatcher = TRUE;
				}
			}
			else
			{
				kMsgTip = "任务有更新！";
				bWatcher = TRUE;
			}
		}
		if(bWatcher)
		{
			vector<string> kWatchers;
			if(QueryIssusWatchers(kWatchers,kIssueKey))
			{
				for(size_t i = 0; i < kWatchers.size(); i++)
				{
					if(kWatchers[i] != kReceiver)
					{
						RtxNotify(kIssue, string("你关注的") + kMsgTip, kIssueKey, kWatchers[i]);
					}
				}
			}
		}
		RtxNotify(kIssue, kMsgTip, kIssueKey, kReceiver);
		kMsgTip.clear();
	}
	m_NotifyIssues.clear();
	return TRUE;
}

BOOL CJIRA2RTXDlg::ParseIssueData(BOOL bUpUpdatedTime/* = TRUE*/,BOOL bNotify/* = TRUE*/)
{
	if(m_kNewData.empty())
	{
		m_StartAt = 0;
		return FALSE;
	}
	if(!ParseIssueFromJson(bUpUpdatedTime, bNotify))
	{
		m_StartAt = 0;
		return FALSE;
	}
	return TRUE;
}

BOOL CJIRA2RTXDlg::UpdateIssue(const Issue& kIssue,BOOL bNotify/* = TRUE*/)
{
	map<INT,Issue>::iterator kIter;
	kIter = m_Issues.find(kIssue.id);
	if(kIter != m_Issues.end())
	{
		if(kIter->second.Cur.updated >= kIssue.Cur.updated)
		{
			return FALSE;
		}
	}
	Issue& kOldIssue = m_Issues[kIssue.id];
	kOldIssue.Old = kOldIssue.Cur;
	kOldIssue.Cur = kIssue.Cur;
	if(kOldIssue.Old.assignee.empty())
	{
		kOldIssue.Old = kOldIssue.Cur;
	}
	kOldIssue.id  = kIssue.id;
	kOldIssue.key = kIssue.key;
	if(bNotify)
	{
		m_NotifyIssues[kIssue.id] = kIssue.id;
	}
	return TRUE;
}

BOOL CJIRA2RTXDlg::QueryUpdatedIssues(BOOL bFirst/* = FALSE*/)
{
	m_kNewData.clear();
	wstring kAddHeaders = _T("Content-Type: application/json");
	string kPostData;
	string kAssignee = "project=" + jrproject + " and ";//"assignee=zhupf and ";
	string kFileds = "\"assignee\",\"reporter\",\"created\",\"updated\",\"duedate\",\"summary\",\"status\",\"resolution\"";
	string kStatus = "(status=" + status_wait + " or status=" + status_running + " or status=" + status_reopen + " or status=" + status_check + " or status=" + status_close + ") ";
	string kUpdated;
	if(!bFirst && !m_LastUpdatedTime.empty())
	{
		kUpdated += "and updated > \'";
		kUpdated += m_LastUpdatedTime + "\' ";
	}
	string kOrder = "order by updated DESC";
	string kStartAt = "\"startAt\":";
	char czStartAt[10];
	sprintf_s(czStartAt,"%d",m_StartAt);
	kStartAt += czStartAt;
	kPostData = "{\"jql\":\"" + kAssignee + kStatus + kUpdated + kOrder + "\",\"fields\":[" + kFileds + "],"+ kStartAt +"}";
	kPostData = AnsiToUtf8(kPostData);
	wstring kServerName = (LPTSTR)CA2W(jrseverip.c_str());
	wstring kReqObj = _T("/rest/api/2/search/?os_username=") + wstring((LPTSTR)CA2W(UrlEncode(os_username).c_str())) + _T("&os_password=") + wstring((LPTSTR)CA2W(UrlEncode(os_password).c_str()));
	WORD wServerPort = jrseverport;
	wstring kAgentName = _T("JIRA2RTX 1.0 By zhupf");

	return HttpQuery(m_kNewData,kAddHeaders,kPostData,kServerName,kReqObj,wServerPort,kAgentName);
}

BOOL CJIRA2RTXDlg::SendNotify(string kReceiver,const string& kMsgTip)
{
	if (g_kUserNameJira2Rtx.find(kReceiver) != g_kUserNameJira2Rtx.end())
	{
		kReceiver = g_kUserNameJira2Rtx[kReceiver];
	}
	m_kNewData.clear();
	wstring kAddHeaders;
	string kPostData;
	kPostData = "title=JIRA任务提示";
	kPostData += "&msg=";
	kPostData += kMsgTip;
	kPostData += "&receiver=";
	kPostData += kReceiver;
	wstring kServerName = (LPTSTR)CA2W(rtxseverip.c_str());;
	wstring kReqObj = _T("/sendnotify.cgi");

	kReqObj += _T("?");
	kReqObj += (LPTSTR)CA2W(kPostData.c_str());
	string kTemp = (LPSTR)CW2A(kReqObj.c_str());
	kReqObj.resize(kTemp.size()+1,NULL);
	size_t iCvt = 0;
	mbstowcs_s(&iCvt,(TCHAR*)kReqObj.c_str(),kReqObj.size(),kTemp.c_str(),kReqObj.size());
	kReqObj = kReqObj.c_str();
	kPostData.clear();

	WORD wServerPort = rtxseverport;
	wstring kAgentName = _T("JIRA2RTX 1.0 By zhupf");
	DWORD dwFlags = WINHTTP_FLAG_ESCAPE_DISABLE|WINHTTP_FLAG_ESCAPE_DISABLE_QUERY|WINHTTP_FLAG_NULL_CODEPAGE;

	return HttpQuery(m_kNewData,kAddHeaders,kPostData,kServerName,kReqObj,wServerPort,kAgentName,dwFlags);
}

BOOL CJIRA2RTXDlg::QueryIssusWatchers(vector<string>& kWatchers,const string& kIssueKey)
{
	kWatchers.clear();
	wstring kAddHeaders;
	string kPostData;
	wstring kServerName = (LPTSTR)CA2W(jrseverip.c_str());
	wstring kReqObj = _T("/rest/api/2/issue/");
	kReqObj += (LPTSTR)CA2W(kIssueKey.c_str());
	kReqObj += _T("/watchers/?os_username=") + wstring((LPTSTR)CA2W(UrlEncode(os_username).c_str())) + _T("&os_password=") + wstring((LPTSTR)CA2W(UrlEncode(os_password).c_str()));
	WORD wServerPort = jrseverport;
	wstring kAgentName = _T("JIRA2RTX 1.0 By zhupf");

	string kWatcherData;
	BOOL bRet = HttpQuery(kWatcherData,kAddHeaders,kPostData,kServerName,kReqObj,wServerPort,kAgentName);
	if(bRet)
	{
		if(!kWatcherData.empty())
		{
			ParseWatcherFromJson(kWatcherData, kWatchers);
		}
	}
	return bRet;
}

BOOL CJIRA2RTXDlg::HttpQuery(
	string& kData,
	const wstring& kAddHeaders,
	const string& kPostData,
	const wstring& kServerName,
	const wstring& kReqObj,
	WORD wServerPort/* = INTERNET_DEFAULT_HTTP_PORT*/,
	const wstring& kAgentName/* = _T("JIRA2RTX 1.0 By zhupf")*/,
	DWORD dwFlags/* = 0*/)
{
	BOOL  bResults = FALSE;
	DWORD dwStatusCodeSize = sizeof(DWORD);
	DWORD dwStatusCode = 0;
	DWORD dwSize = 0;
	DWORD dwDownloaded = 0;
	LPSTR pszOutBuffer;
	HINTERNET  hSession = NULL;
	HINTERNET hConnect = NULL;
	HINTERNET hRequest = NULL;

	// Use WinHttpOpen to obtain a session handle.
	hSession = WinHttpOpen( kAgentName.c_str(), 
		WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
		WINHTTP_NO_PROXY_NAME,
		WINHTTP_NO_PROXY_BYPASS, 0);
	if(!hSession)
	{
		TRACE( _T("Error %u in WinHttpOpen.\n"), GetLastError());
	}

	// Use WinHttpSetTimeouts to set a new time-out values.
	if (!WinHttpSetTimeouts( hSession, 0, 1000, 1000, 1000))
		TRACE( _T("Error %u in WinHttpSetTimeouts.\n"), GetLastError());

	// Specify an HTTP server.
	if (hSession)
	{
		hConnect = WinHttpConnect( hSession, kServerName.c_str(), wServerPort, 0);
		if(!hConnect)
		{
			TRACE( _T("Error %u in WinHttpConnect.\n"), GetLastError());
		}
	}

	// Create an HTTP request handle.
	LPCWSTR pwszObjectName = NULL;
	if (!kReqObj.empty())
	{
		pwszObjectName = kReqObj.c_str();
	}
	if (hConnect)
	{
		wstring kVerb = _T("GET");
		if(!kPostData.empty())
		{
			kVerb = _T("POST");
		}
		hRequest = WinHttpOpenRequest( hConnect, kVerb.c_str(), pwszObjectName,
			NULL, WINHTTP_NO_REFERER, 
			WINHTTP_DEFAULT_ACCEPT_TYPES, 
			dwFlags);
	}
	if( hRequest && !kAddHeaders.empty())
	{
		bResults = WinHttpAddRequestHeaders( hRequest, 
			kAddHeaders.c_str(),//_T("Content-Type: application/json")
			kAddHeaders.size(),
			WINHTTP_ADDREQ_FLAG_ADD );
	}
	if (hRequest)
	{
		bResults = WinHttpSendRequest( hRequest,
			WINHTTP_NO_ADDITIONAL_HEADERS,
			0, (LPVOID)kPostData.c_str(), kPostData.size(), 
			kPostData.size(), 0);
		if(!bResults)
		{
			TRACE( _T("Error %u in WinHttpSendRequest.\n"), GetLastError());
		}
	}
	if (bResults)
	{
		bResults = WinHttpReceiveResponse( hRequest, NULL);
		if(!bResults)
		{
			TRACE( _T("Error %u in WinHttpReceiveResponse.\n"), GetLastError());
		}
	}
	if( bResults )
	{
		bResults = WinHttpQueryHeaders( hRequest, 
			WINHTTP_QUERY_STATUS_CODE | WINHTTP_QUERY_FLAG_NUMBER,
			NULL, 
			&dwStatusCode,
			&dwStatusCodeSize,
			WINHTTP_NO_HEADER_INDEX);
		if(!bResults)
		{
			TRACE( _T("Error %u in WinHttpQueryHeaders.\n"), GetLastError());
		}
	}
	if (bResults)
	{
		do 
		{
			dwSize = 0;
			if (!WinHttpQueryDataAvailable( hRequest, &dwSize))
			{
				TRACE( _T("Error %u in WinHttpQueryDataAvailable.\n"), GetLastError());
				bResults = FALSE;
			}
			if(dwSize == 0)
			{
				break;
			}
			pszOutBuffer = new char[dwSize+1];
			if (!pszOutBuffer)
			{
				TRACE(_T("Out of memory\n"));
				dwSize=0;
				bResults = FALSE;
				break;
			}
			ZeroMemory(pszOutBuffer, dwSize+1);
			if (!WinHttpReadData( hRequest, (LPVOID)pszOutBuffer, dwSize, &dwDownloaded))
			{
				TRACE( _T("Error %u in WinHttpReadData.\n"), GetLastError());
				dwSize=0;
				bResults = FALSE;
			}
			else
			{
				kData.append(pszOutBuffer);
			}
			delete [] pszOutBuffer;

		} while (dwSize > 0);
	}
	if (!bResults)
	{
		TRACE( _T("Error %d has occurred.\n"), GetLastError());
		kData.clear();
	}
	if(dwStatusCode != 200 && dwStatusCode != 206)
	{
		TRACE( _T("Error StatusCode %d has occurred.\n"), dwStatusCode);
		bResults = FALSE;
		kData = Utf8ToAnsi(kData);
		m_LogList.AddString((LPTSTR)CA2T(kData.c_str()));
		kData.clear();
	}
	if (hRequest) WinHttpCloseHandle(hRequest);
	if (hConnect) WinHttpCloseHandle(hConnect);
	if (hSession) WinHttpCloseHandle(hSession);
	return bResults;
}

BOOL CJIRA2RTXDlg::ParseIssueFromJson(BOOL bUpUpdatedTime, BOOL bNotify)
{
	Document kDoc;
	if (kDoc.ParseInsitu<0>((char*)m_kNewData.c_str()).HasParseError())
	{
		return FALSE;
	}
	int startAt = kDoc["startAt"].GetInt();
	int maxResults = kDoc["maxResults"].GetInt();
	int total = kDoc["total"].GetInt();
	if(total > maxResults && (startAt + maxResults) < total)
	{
		m_StartAt = startAt + maxResults;
	}
	else
	{
		m_StartAt = 0;
	}
	const Value& issues = kDoc["issues"];
	for(size_t i = 0; i < issues.Size(); i++)
	{
		Issue kIssue;
		const Value& issue = issues[i];
		kIssue.id = atol(Utf8ToAnsi(issue["id"].GetString()).c_str());
		kIssue.key = Utf8ToAnsi(issue["key"].GetString());

		const Value& fields		= issue["fields"];
		const Value& assignee	= fields["assignee"];
		const Value& reporter	= fields["reporter"];
		const Value& status		= fields["status"];
		const Value& resolution = fields["resolution"];
		kIssue.Cur.assignee		= Utf8ToAnsi(assignee["name"].GetString());
		kIssue.Cur.reporter		= Utf8ToAnsi(reporter["name"].GetString());
		kIssue.Cur.status		= Utf8ToAnsi(status["name"].GetString());
		kIssue.Cur.resolution	= Utf8ToAnsi(resolution["name"].GetString());
		kIssue.Cur.created		= Utf8ToAnsi(fields["created"].GetString());
		kIssue.Cur.updated		= Utf8ToAnsi(fields["updated"].GetString());
		kIssue.Cur.duedate		= Utf8ToAnsi(fields["duedate"].GetString());
		kIssue.Cur.summary		= Utf8ToAnsi(fields["summary"].GetString());
		if(i == 0 && bUpUpdatedTime)
		{
			m_LastUpdatedTime = kIssue.Cur.updated.substr(0,16);
			m_LastUpdatedTime[10] = ' ';
		}
		UpdateIssue(kIssue,bNotify);
	}
	return TRUE;
}

BOOL CJIRA2RTXDlg::ParseWatcherFromJson(string kWatcherData, vector<string>& kWatchers)
{
	Document kDoc;
	if (kDoc.ParseInsitu<0>((char*)kWatcherData.c_str()).HasParseError())
	{
		return FALSE;
	}
	string watcherName;
	int watchCount = kDoc["watchCount"].GetInt();
	const Value& watchers = kDoc["watchers"];
	for(int i = 0; i < watchCount; i++)
	{
		const Value& watcher = watchers[i];
		watcherName = Utf8ToAnsi(watcher["name"].GetString());
		kWatchers.push_back(watcherName);
	}
	return TRUE;
}
