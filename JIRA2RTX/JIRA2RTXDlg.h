
// JIRA2RTXDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "Winhttp.h"

struct IssueData
{
	string assignee;
	string reporter;
	string status;
	string resolution;
	string created;
	string updated;
	string duedate;
	string summary;
};
struct Issue
{
	INT id;
	string key;
	IssueData Cur;
	IssueData Old;
};

// CJIRA2RTXDlg 对话框
class CJIRA2RTXDlg : public CDialogEx
{
// 构造
public:
	CJIRA2RTXDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_JIRA2RTX_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public:
	BOOL QueryUpdatedIssues(BOOL bFirst = FALSE);
	BOOL ParseIssueData(BOOL bUpUpdatedTime = TRUE,BOOL bNotify = TRUE);
	BOOL ParseIssueFromJson(BOOL bUpUpdatedTime, BOOL bNotify);
	BOOL UpdateIssue(const Issue& kIssue,BOOL bNotify = TRUE);
	BOOL SendAllNotify();
	BOOL RtxNotify(const Issue& kIssue,string kMsgTip, const string& kIssueKey, const string& kReceiver);
	BOOL QueryIssusWatchers(vector<string>& kWatchers,const string& kIssueKey);
	BOOL ParseWatcherFromJson(string kWatcherData, vector<string>& kWatchers);

	BOOL HttpQuery(
		string& kData,
		const wstring& kAddHeaders,
		const string& kPostData,
		const wstring& kServerName,
		const wstring& kReqObj,
		WORD wServerPort = INTERNET_DEFAULT_HTTP_PORT,
		const wstring& kAgentName = _T("JIRA2RTX 1.0 By zhupf"),
		DWORD dwFlags = 0);

	BOOL SendNotify(string kReceiver,const string& kMsgTip);
	void DoResolveWorkThread();
public:
	string m_LastUpdatedTime;
	string m_kNewData;
	INT m_StartAt;
	map<INT,Issue> m_Issues;
	map<INT,INT> m_NotifyIssues;

// 实现
protected:
	HICON m_hIcon;
	CButton m_BtnRun;
	CListBox m_LogList;
	CRect m_WndRect;
	CRect m_RectBtnRun;
	CRect m_RectLogList;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonRun();
	afx_msg LRESULT OnAddLog(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
